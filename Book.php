<?php

class Book extends Product
{
	protected $weight;
	
	public function __construct($SKU , $NAME , $PRICE , $WEIGHT)
	{
		$this->sku = $SKU;
		$this->name = $NAME;
		$this->price = $PRICE;
		$this->weight = $WEIGHT;
		$this->type = "Book";
		$this->value = $WEIGHT;
	}
	
	function isValid()
	{
		return $this->productIsValid() && is_numeric($this->weight);
	}
}