function changeType()
{
	var type = document.getElementById("productType");
	if(type.value == "DVD")
	{
		document.getElementById("o1").style.display = "table";
		document.getElementById("o2").style.display = "none";
		document.getElementById("o3").style.display = "none";
	}
	else if(type.value == "Furniture")
	{
		document.getElementById("o1").style.display = "none";
		document.getElementById("o2").style.display = "table";
		document.getElementById("o3").style.display = "none";
	}
	else if(type.value == "Book")
	{
		document.getElementById("o1").style.display = "none";
		document.getElementById("o2").style.display = "none";
		document.getElementById("o3").style.display = "table";
	}
}

function submitForm()
{
	if(verifyInput())
	document.getElementById('product_form').submit();
}

function verifyInput()
{
	var SKU = document.getElementById('sku').value;
	var Name = document.getElementById('name').value;
	var Price = document.getElementById('price').value;
	var Type = document.getElementById('productType').value;
	var error = "";
	
	
	if(!SKU || SKU.length === 0)
		error += "SKU field is empty<br>";
	if(!Name || Name.length === 0)
		error += "Name field is empty<br>";
	if(!Price || Price.length === 0)
		error += "Price field is empty<br>";
	if(isNaN(Price))
		error += "Price field should be a number<br>";
	
	if(Type == "DVD")
	{
		var Size = document.getElementById('size').value;
		if(!Size || Size.length === 0)
		error += "Size field is empty<br>";
		else if(isNaN(Size))
		error += "Size field should be a number<br>";
	}
	else if(Type == "Furniture")
	{
		var Height = document.getElementById('height').value;
		var Width = document.getElementById('width').value;
		var Length = document.getElementById('length').value;
		if(!Height || Height.length === 0)
		  error += "Height field is empty<br>";
		else if(isNaN(Height))
		error += "Height field should be a number<br>";
	    if(!Width || Width.length === 0)
		  error += "Width field is empty<br>";
	 else if(isNaN(Width))
		error += "Width field should be a number<br>";
	    if(!Length || Length.length === 0)
		  error += "Length field is empty<br>";
		else if(isNaN(Length))
			error += "Length field should be a number<br>";
	}
	else if(Type == "Book")
	{
		var Weight = document.getElementById('weight').value;
		if(!Weight || Weight.length === 0)
		  error += "Weight field is empty<br>";
	    else if(isNaN(Weight))
			error += "Weight field should be a number<br>";
	}
	
	document.getElementById("errors").innerHTML = error;
	
	if(!error || error.length === 0) {
		document.getElementById("errors").style.display = "none";
		return true;
	}
	document.getElementById("errors").style.display = "block";
	return false;
}