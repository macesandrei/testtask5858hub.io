<?php

class DVD extends Product
{
	protected $size;
	public function __construct($SKU , $NAME , $PRICE , $SIZE)
	{
		$this->sku = $SKU;
		$this->name = $NAME;
		$this->price = $PRICE;
		$this->weight = $SIZE;
		$this->type = "DVD";
		$this->size = $SIZE;
		$this->value = $SIZE;
	}
	
	public function isValid()
	{
		return $this->productIsValid() && is_numeric($this->size);
	}
	
}