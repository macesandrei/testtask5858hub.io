<!DOCTYPE HTML>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="style.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="jquery-3.5.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
	<script src="index.js"></script>
  </head>
  <body>
    <div class="container-fluid">
		<header>
		  <h2>Product List</h2>
		  <div class="buttons">
		    <a class="btn btn-primary" href="add-product.php">ADD</a>
		    <form class="btn" id="delete_form" action="index.php" method="post"><input type="submit" value="MASS DELETE" class="btn btn-primary" /></form>
		  </div>
		</header>
		<hr>
		
		<?php
		
		$db = mysqli_connect('localhost', 'id17751187_root', 'Andrei-Maces1234', 'id17751187_products');
		if (isset($_POST['object'])) {
		$objects = $_POST['object'];
		foreach ($objects as &$object)
		{
			$sql = "DELETE FROM product WHERE ID = $object";
			mysqli_query($db , $sql);
		}
		}
		$results = mysqli_query($db, "SELECT * FROM product");
		?>
		
		<div class="product-list">
		<?php while ($row = mysqli_fetch_array($results)) { ?>
		  <div class="product" id="<?php echo $row['ID']; ?>">
		    <input type="checkbox" class="delete-checkbox" form="delete_form" value=<?php echo $row['ID']?> name="object[]"/>
			<div class="product-content">
			  <h4><?php echo $row['SKU']; ?></h4>
			  <h4><?php echo $row['Name']; ?></h4>
			  <h4><?php echo $row['Price']; ?> $</h4>
			  <h4><?php 
				if($row['Type'] == "DVD")
				  echo $row['Value'] . " MB";
				else if($row['Type'] == "Furniture")
					echo $row['Value'];
				else if($row['Type'] == "Book")
					echo $row['Value'] . " KG";
			  ?></h4>
			</div>
		  </div>
		<?php } ?>
		</div>
		<hr>
		<footer>
		  <h3>Scandiweb Test assigment</h3>
		</footer>
	</div>
  </body>
</html>