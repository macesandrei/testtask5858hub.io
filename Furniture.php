<?php

class Furniture extends Product
{
	protected $height;
	protected $width;
	protected $length;
	
	public function __construct($SKU , $NAME , $PRICE , $HEIGHT , $WIDTH , $LENGTH)
	{
		$this->sku = $SKU;
		$this->name = $NAME;
		$this->price = $PRICE;
		$this->height = $HEIGHT;
		$this->width = $WIDTH;
		$this->length = $LENGTH;
		$this->type = "Furniture";
		$this->value = $this->height . 'x' . $this->width . 'x' . $this->length;
	}
	
	public function isValid()
	{
		return $this->productIsValid() && is_numeric($this->width) && is_numeric($this->height) && is_numeric($this->length);
	}
}