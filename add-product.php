<!DOCTYPE HTML>
<html>
  <head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
	<script src="index.js"></script>
	<link rel="stylesheet" type="text/css" href="style.css" />
  </head>
  <body onload="changeType()">
    <div class="container-fluid">
		<header>
		  <h2>Product Add</h2>
		  <div class="buttons">
		    <button class="btn btn-primary" onclick="submitForm()">Save</button>
		    <a class="btn btn-primary" href="index.php">Cancel</a>
		  </div>
		</header>
		<hr>
		<form id="product_form" action="add-product.php" method="post">
		  <table>
		    <tr>
			  <td><label for="SKU">SKU: </label></td>
			  <td><input id="sku" type="text" name="SKU"/></td>
			</tr>
		    <tr>
			  <td><label for="NAME">Name: </label></td>
			  <td><input id="name" type="text" name="Name"/></td>
			</tr>
		    <tr>
			  <td><label for="PRICE">Price ($): </label></td>
			  <td><input id="price" type="text" name="Price"/></td>
			</tr>
		    <tr>
			  <td><label for="TYPE">Type Switcher: </label></td>
			  <td>
			    <select id="productType" name="Type" onchange="changeType()">
				  <option value="DVD" id="DVD">DVD</option>
				  <option value="Furniture" id="Furniture">Furniture</option>
				  <option value="Book" id="Book">Book</option>
				</select>
		      </td>
			</tr>
		  </table>
		  
		  <table id="o1">
			<tr>
			  <td><label for="Size">Size (MB): </label></td>
			  <td><input type="number" id="size" name="Size"/></td>
			</tr>
		  </table>
		  
		  <table id="o2">
			<tr>
			  <td><label>Height (CM): </label></td>
			  <td><input type="number" id="height" name="Height"/></td>
			</tr>
			<tr>
			  <td><label>Width (CM): </label></td>
			  <td><input type="number" id="width" name="Width"/></td>
			</tr>
			<tr>
			  <td><label>Length (CM): </label></td>
			  <td><input type="number" id="length" name="Length"/></td>
			</tr>
		  </table>
		  
		  <table id="o3">
		    <tr>
			  <td><label>Weight (KG): </label></td>
			  <td><input type="number" id="weight" name="Weight"/></td>
			</tr>
		  </table>
		  
		  <?php
			include 'Product.php';
			include 'Book.php';
			include 'Furniture.php';
			include 'DVD.php';
			
		$db = mysqli_connect('localhost', 'id17751187_root', 'Andrei-Maces1234', 'id17751187_products');
			
			if(!empty($_POST['SKU'])){
				
				$Type = $_POST['Type'];
				$SKU = $_POST['SKU'];
				$Name = $_POST['Name'];
				$Price = $_POST['Price'];
				
			if($Type == "Book")
			{
				$product = new Book($SKU , $Name , $Price , $_POST['Weight']);
				if($product->isValid())
					$product->INSERT($db);
			}
			else if($Type == "DVD")
			{
				$product = new DVD($SKU , $Name , $Price , $_POST['Size']);
				if($product->isValid())
					$product->INSERT($db);
			}
			else if($Type == "Furniture") {
				$product = new Furniture($SKU , $Name , $Price , $_POST['Height'] , $_POST['Width'] , $_POST['Length']);
				if($product->isValid())
					$product->INSERT($db);
			}}
			?>
	
		</form>
		<div class="alert alert-danger" id="errors" style="display: none;">
		</div>
		<hr>
		<footer>
		  <h3>Scandiweb Test assigment</h3>
		</footer>
	</div>
  </body>
</html>